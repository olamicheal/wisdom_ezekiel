$(window).on("load", function () {
    "use strict";
    $(".loader").fadeOut(800);
    $('.side-menu').removeClass('opacity-0');
    
});   


let sideMenuToggle = $("#sidemenu_toggle");
let sideMenu = $(".side-menu");
if (sideMenuToggle.length) {
    sideMenuToggle.on("click", function () {
        $("body").addClass("overflow-hidden");
        sideMenu.addClass("side-menu-active");
        $(function () {
            setTimeout(function () {
                $("#close_side_menu").fadeIn(300);
            }, 300);
        });
    });
    $("#close_side_menu , #btn_sideNavClose , .side-nav .nav-link.pagescroll").on("click", function () {
        $("body").removeClass("overflow-hidden");
        sideMenu.removeClass("side-menu-active");
        $("#close_side_menu").fadeOut(200);
        $(() => {
            setTimeout(() => {
                $('.sideNavPages').removeClass('show');
                $('.fas').removeClass('rotate-180');
            }, 400);
        });
    });
    $(document).keyup(e => {
        if (e.keyCode === 27) { // escape key maps to keycode `27`
            if (sideMenu.hasClass("side-menu-active")) {
                $("body").removeClass("overflow-hidden");
                sideMenu.removeClass("side-menu-active");
                $("#close_side_menu").fadeOut(200);
                // $tooltip.tooltipster('close');
                $(() => {
                    setTimeout(()=> {
                        $('.sideNavPages').removeClass('show');
                        $('.fas').removeClass('rotate-180');
                    }, 400);
                });
            }
        }
    });
}

  /*
     * Side menu collapse opener
     * */
    $(".collapsePagesSideMenu").on('click', function () {
        $(this).children().toggleClass("rotate-180");
    });

