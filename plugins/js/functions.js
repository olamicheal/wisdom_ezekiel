/* ===================================
    About
====================================== */

/*---------------------------------------------------------------------
    Theme Name: Trax
    Theme URI:
    Author: Themes Industry
    Author URI:
    Description: One Page , Multi Parallax Template
    Tags: one page, multi page, multipurpose, parallax, creative, html5

 ----------------------------------------------------------------------*/

//PAGE LOADER
$(window).on("load", function () {
    "use strict";
    $(".loader").fadeOut(800);
    $('.side-menu').removeClass('opacity-0');
});
// mentorship form submission end-point
let url = 'http://wisdomezekiel.org/server/server.php';
// getting the vhrckboxes values
let avenue;
function social(data){avenue = data; };
let liketobementored;
function mentoring(data) {liketobementored = data};
let ageRange;
function age(data){ageRange = data};
let sex;
function gender(data){sex = data};
let employmentstatus ;
function workstatus(data){employmentstatus = data}
function submitForm(){
    // getting values from the input fields
    let fullname = document.getElementById('name').value;
    let email = document.getElementById('email').value;
    let phonenumber = document.getElementById('phone').value;
    let expect = document.getElementById('expect').value;
    let occupation = document.getElementById('occupation').value;

    let data = {fullname, sex, ageRange, employmentstatus,
    occupation, whattoexpect: expect, phonenumber, avenue,
    email, liketobementored}
    // checking through required fields
    for (var x in data) {
      if(data[x].length < 1){
        $("#error").slideDown()
        setTimeout(function(){ $("#error").slideUp();; }, 7000);
        return false;
      }
    }
    $(".loader").fadeIn();
    // request to the server
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function() {
            $("#alert").slideDown()
            setTimeout(function(){ $("#alert").slideUp();; }, 7000);
          },
    });
    $(".loader").fadeOut(1000);
    // clearing all fields
    document.getElementById('name').value = ' '
    document.getElementById('email').value = ' ';
    document.getElementById('phone').value = ' ';
    document.getElementById('expect').value = '';
    document.getElementById('occupation').value = ' ' ;
    document.getElementById("male").checked = false;
    document.getElementById("female").checked = false;
    document.getElementById("employed").checked = false;
    document.getElementById("unemployed").checked = false;
    document.getElementById("agerange1").checked = false;
    document.getElementById("agerange2").checked = false;
    document.getElementById("agerange3").checked = false;
    document.getElementById("agerange4").checked = false;
    document.getElementById("mentor1").checked = false;
    document.getElementById("mentor2").checked = false;
    document.getElementById("instagram").checked = false;
    document.getElementById("facebook").checked = false;
    document.getElementById("whatsapp").checked = false;
    document.getElementById("google").checked = false;
    document.getElementById("othersß").checked = false;  
}


jQuery($=> {
    "use strict";
    let $window = $(window);
    let body = $("body");
    let $root = $("html, body");
    // $('[data-toggle="tooltip"]').tooltip();

    //contact us
    $("#submit_btn1 , #submit_btn").on('click', function () {
        let userName = $('#name1').val();
        let userEmail = $('#email1').val();
        let userMessage = $('#message1').val();
        let result;
        if(this.id === 'submit_btn'){
            result = $('#result');
            userMessage = $('#companyName').val();
            userName = $('#userName').val();
            userEmail = $('#email').val();
        }
        else{
            result = $('#result1');
        }
        //simple validation at client's end
        let postData, output;
        let proceed = true;
        if (userName === "") {
            proceed = false;
        }
        if (userEmail === "") {
            proceed = false;
        }
        if (userMessage === "") {
            proceed = false;
        }
        //everything looks good! proceed...
        if (proceed) {

            //data to be sent to server
            postData = {
                'userName': userName,
                'userEmail': userEmail,
                'userMessage': userMessage
            };

            //Ajax post data to server
            $.post('contact.php', postData, function (response) {
                //load json data from server and output message
                if (response.type === 'error') {
                    output = '<div class="alert-danger" style="padding:10px; margin-bottom:25px;">' + response.text + '</div>';
                } else {
                    output = '<div class="alert-success" style="padding:10px; margin-bottom:25px;">' + response.text + '</div>';
                    //reset values in all input fields
                    $('.getin_form input').val('');
                    $('.getin_form textarea').val('');

                }

                result.slideUp("fast").html(output).slideDown();
            }, 'json');

        } else {
            output = '<div class="alert-danger" style="padding:10px; margin-bottom:25px;">Please provide the missing fields.</div>';
            result.slideUp("fast").html(output).slideDown();
        }

    });
    /*rating stars*/
    let fadeInStar = () => {
        let starItem = $('#rattingIcon .fa-star.fas');
        starItem.addClass('scale-star');
        setTimeout(function () {
            starItem.removeClass('scale-star');
        }, 180);
    }
    let ratingText = $('#ratingText');

    let fadeInStarText = n => {
        ratingText.addClass('scale-price');
        setTimeout(function () {
            ratingText.removeClass('scale-price');
            switch (n) {
                case 0:
                    ratingText.text('Poor!');
                    break;
                case 1:
                    ratingText.text('Average!');
                    break;
                case 2:
                    ratingText.text('Good!');
                    break;
                case 3:
                    ratingText.text('Very Good!');
                    break;
                case 4:
                    ratingText.text('Excellent!');
            }
        }, 180);
    }

    $("#rattingIcon .fa-star").on('click', function () {
        let iconIndex = $(this).index();
        $(this).addClass("fas").removeClass("far");
        $(this).prevAll().addClass("fas").removeClass("far");
        $(this).nextAll().addClass("far").removeClass("fas");
        fadeInStar();
        fadeInStarText(iconIndex);
    });


    /* ----- Back to Top ----- */
    $(body).append('<a href="#" class="back-top"><i class="fa fa-angle-up"></i></a>');
    let amountScrolled = 700;
    let backBtn = $("a.back-top");
    $window.on("scroll", function () {
        if ($window.scrollTop() > amountScrolled) {
            backBtn.addClass("back-top-visible");
        } else {
            backBtn.removeClass("back-top-visible");
        }
    });
    backBtn.on("click", function () {
        $root.animate({
            scrollTop: 0
        }, 700);
        return false;
    });

    /* ------- Smooth scroll ------- */
    $("a.pagescroll").on("click", function (event) {
        event.preventDefault();
        let action = $(this.hash).offset().top;
        if($(this).hasClass('scrollupto')){
            action-=45;
        }
        $("html,body").animate({
            scrollTop: action
        }, 1200);
    });

    /* ------- navbar menu Position dynamically ------- */
    $(".dropdown").on("mouseenter", function () {
        let $elem = $(this).find('.dropdown-menu'),
            left = $elem.offset().left,
            width = $elem.width(),
            docW = $(window).width();

        if ((left + width) > docW) {
            $elem.addClass("right-show");
        } else if ((left + (width * 2)) < docW) {
            $elem.removeClass("right-show");
        }
    });

    /*------ Sticky MENU Fixed ------*/
    let headerHeight = $("header").outerHeight();
    let navbar = $("nav.navbar");
    if (navbar.not('.fixed-bottom').hasClass("static-nav")) {
        $window.scroll(function () {
            let $scroll = $window.scrollTop();
            let $navbar = $(".static-nav");
            let nextSection = $(".section-nav-smooth");
            if ($scroll > 50) {
                $navbar.addClass("fixedmenu");
                nextSection.css("margin-top", headerHeight);
            } else {
                $navbar.removeClass("fixedmenu");
                nextSection.css("margin-top", 0);
            }
            if ($scroll > 50) {
                $('.header-with-topbar nav').addClass('mt-0');
            } else {
                $('.header-with-topbar nav').removeClass('mt-0');
            }
        });
        $(function () {
            if ($window.scrollTop() >= $(window).height()) {
                $(".static-nav").addClass('fixedmenu');
            }
        })
    }
    if (navbar.hasClass("fixed-bottom")) {
        let navTopMargin = $(".fixed-bottom").offset().top;
        let scrollTop = $window.scrollTop();
        $(window).scroll(function () {
            if ($(window).scrollTop() > navTopMargin) {
                $('.fixed-bottom').addClass('fixedmenu');
            } else {
                $('.fixed-bottom').removeClass('fixedmenu');
            }
            if ($(window).scrollTop() < 260) {
                $('.fixed-bottom').addClass('menu-top');
            } else {
                $('.fixed-bottom').removeClass('menu-top');
            }
        });
        $(function () {
            if (scrollTop < 230) {
                $('.fixed-bottom').addClass('menu-top');
            } else {
                $('.fixed-bottom').removeClass('menu-top');
            }
            if (scrollTop >= $(window).height()) {
                $('.fixed-bottom').addClass('fixedmenu');
            }
        })
    }
    /*Menu Onclick*/
    let sideMenuToggle = $("#sidemenu_toggle");
    let sideMenu = $(".side-menu");
    if (sideMenuToggle.length) {
        sideMenuToggle.on("click", function () {
            $("body").addClass("overflow-hidden");
            sideMenu.addClass("side-menu-active");
            $(function () {
                setTimeout(function () {
                    $("#close_side_menu").fadeIn(300);
                }, 300);
            });
        });
        $("#close_side_menu , #btn_sideNavClose , .side-nav .nav-link.pagescroll").on("click", function () {
            $("body").removeClass("overflow-hidden");
            sideMenu.removeClass("side-menu-active");
            $("#close_side_menu").fadeOut(200);
            $(() => {
                setTimeout(() => {
                    $('.sideNavPages').removeClass('show');
                    $('.fas').removeClass('rotate-180');
                }, 400);
            });
        });
        $(document).keyup(e => {
            if (e.keyCode === 27) { // escape key maps to keycode `27`
                if (sideMenu.hasClass("side-menu-active")) {
                    $("body").removeClass("overflow-hidden");
                    sideMenu.removeClass("side-menu-active");
                    $("#close_side_menu").fadeOut(200);
                    // $tooltip.tooltipster('close');
                    $(() => {
                        setTimeout(()=> {
                            $('.sideNavPages').removeClass('show');
                            $('.fas').removeClass('rotate-180');
                        }, 400);
                    });
                }
            }
        });
    }

    /*
     * Side menu collapse opener
     * */
    $(".collapsePagesSideMenu").on('click', function () {
        $(this).children().toggleClass("rotate-180");
    });


    /* ----- Full Screen ----- */
    let resizebanner = ()=> {
        let $fullscreen = $(".full-screen");
        $fullscreen.css("height", $window.height());
        $fullscreen.css("width", $window.width());
    }
    resizebanner();
    $window.resize(function () {
        resizebanner();
    });

    $('.progress').each(function () {
        $(this).appear(function () {
            $(this).animate({
                opacity: 1,
                left: "0px"
            }, 500);
            let b = jQuery(this).find(".progress-bar").attr("data-value");
            $(this).find(".progress-bar").animate({
                width: b + "%"
            }, 500);
        });
    });

    /*----- shop detail Tabs init -----*/
    $(function () {
        initTabsToAccordion();
    });

    function initTabsToAccordion() {
        var animSpeed = 500;
        var win = $(window);
        var isAccordionMode = true;
        var tabWrap = $(".tab-to-accordion");
        var tabContainer = tabWrap.find(".tab-container");
        var tabItem = tabContainer.children("div[id]");
        var tabsetList = tabWrap.find(".tabset-list");
        var tabsetLi = tabsetList.find("li");
        var tabsetItem = tabsetList.find("a");
        var activeId = tabsetList
            .find(".active")
            .children()
            .attr("href");
        cloneTabsToAccordion();
        accordionMode();
        tabsToggle();
        hashToggle();
        win.on("resize orientationchange", accordionMode);

        function cloneTabsToAccordion() {
            $(tabsetItem).each(function () {
                var $this = $(this);
                var activeClass = $this.parent().hasClass("active");
                var listItem = $this.attr("href");
                var listTab = $(listItem);
                if (activeClass) {
                    var activeClassId = listItem;
                    listTab.show();
                }
                var itemContent = $this.clone();
                var itemTab = $this.attr("href");
                if (activeClassId) {
                    itemContent
                        .insertBefore(itemTab)
                        .wrap('<div class="accordion-item active"></div>');
                } else {
                    itemContent
                        .insertBefore(itemTab)
                        .wrap('<div class="accordion-item"></div>');
                }
            });
        }

        function accordionMode() {
            var liWidth = Math.round(tabsetLi.outerWidth());
            var liCount = tabsetLi.length;
            var allLiWidth = liWidth * liCount;
            var tabsetListWidth = tabsetList.outerWidth();
            if (tabsetListWidth <= allLiWidth) {
                isAccordionMode = true;
                tabWrap.addClass("accordion-mod");
            } else {
                isAccordionMode = false;
                tabWrap.removeClass("accordion-mod");
            }
        }

        function tabsToggle() {
            tabItem.hide();
            $(activeId).show();
            $(tabWrap).on("click", 'a[href^="#tab"]', function (e) {
                e.preventDefault();
                var $this = $(this);
                var activeId = $this.attr("href");
                var activeTabSlide = $(activeId);
                var activeOpener = tabWrap.find('a[href="' + activeId + '"]');
                $('a[href^="#tab"]')
                    .parent()
                    .removeClass("active");
                activeOpener.parent().addClass("active");
                if (isAccordionMode) {
                    tabItem.stop().slideUp(animSpeed);
                    activeTabSlide.stop().slideDown(animSpeed);
                } else {
                    tabItem.hide();
                    activeTabSlide.show();
                }
            });
        }

        function hashToggle() {
            var hash = location.hash;
            var activeId = hash;
            var activeTabSlide = $(activeId);
            var activeOpener = tabWrap.find('a[href="' + activeId + '"]');
            if ($(hash).length > 0) {
                $('a[href^="#tab"]')
                    .parent()
                    .removeClass("active");
                activeOpener.parent().addClass("active");
                tabItem.hide();
                activeTabSlide.show();
                win
                    .scrollTop(activeTabSlide.offset().top)
                    .scrollLeft(activeTabSlide.offset().left);
            }
        }
    }
    /* =====================================
             Particles Index
      ====================================== */

    if ($("#particles-js").length) {
        particlesJS('particles-js', {
            "particles": {
                "number": {
                    "value": 100,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "#ffffff"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 5,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": false,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 2,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "bounce",
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "grab"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "bubble"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 150,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 150,
                        "size": 12,
                        "duration": 0.2,
                        "opacity": 0.6,
                        "speed": 10
                    },
                    "repulse": {
                        "distance": 150
                    },
                    "push": {
                        "particles_nb": 1
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true,
        });
    }

    /* =====================================
             Text Rotating in Particles
      ====================================== */
    let morphRotatingText = $("#morph-text");
    if (morphRotatingText.length) {
        morphRotatingText.Morphext({
            // The [in] animation type. Refer to Animate.css for a list of available animations.
            animation: "flipInX",
            // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
            separator: ",",
            // The delay between the changing of each phrase in milliseconds.
            speed: 3500,
            complete: function () {
                // Called after the entrance animation is executed.
            }
        });
    }

    /* =====================================
     Parallax And responsive plugins initialize
      ====================================== */
    // let $tooltip = $('.tooltip');
    // $(()=> {
    //     $tooltip.tooltipster({
    //         plugins: ['follower'],
    //         anchor: 'bottom-right',
    //         offset: [0, 0],
    //         animation: 'fade',
    //         content:'Click Here To Close or Press ESC!',
    //         delay: 20,
    //         theme: 'tooltipster-light',
    //         repositionOnScroll: true,
    //         // change the content of tooltip in all pages
    //         // functionBefore: function (instance, helper) {
    //         //     instance.content('Click Here To Close or Press ESC!');
    //         // }
    //     });
    // });
    /*Wow Animations*/
    if ($(".wow").length && $(window).outerWidth() >= 567) {
        let wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: false,
            live: true
        });
        wow.init();
    }

    /* =====================================
                 Pricing duration toggle
          ====================================== */

    $('.Pricing-toggle-button').on('click', function () {
        var opt = true;
        if ($(this).hasClass('month')) {
            opt = false;
        }
        if (!$(this).hasClass('active')) {
            $('.pricing-price .pricing-currency').each(function () {
                let priceWithDollar = $(this).text();
                let price = priceWithDollar.substring(1, priceWithDollar.length);
                var discountOffer = 9;
                if (opt) {
                    price *= discountOffer;
                } else {
                    price /= discountOffer;
                }
                price = price.toFixed(2);
                let priceF = '$' + price;
                fadeInPrice($(this), priceF);
            });
            $('.pricing-price .pricing-duration').each(function () {
                if (opt) {
                    $(this).text("year");
                } else {
                    $(this).text("month");
                }

            });
            $(this).addClass('active').siblings().removeClass('active');
        }
    });

    let fadeInPrice = (thisItem, priceText)=> {
        let pricingItem = $('.pricing-price');
        pricingItem.addClass('scale-price');
        setTimeout(function () {
            thisItem.text(priceText);
            pricingItem.removeClass('scale-price');
        }, 200);
    }

    $('.pricing-item').on('mouseenter' , function(){
       $('.pricing-item').removeClass('active');
        $(this).addClass('active');
    }).on('mouseleave' , function(){
        $('.pricing-item').removeClass('active');
        $('.pricing-item.selected').addClass('active');
    });

    //shop detail dual carousel
    let syncCont = $("#shop-dual-carousel");
    let syncCarousel = $("#syncCarousel.owl-carousel");

    if (syncCont) {
        syncCont.append('<div class="owl-carousel carousel-shop-detail-inner owl-theme" id="syncChild"></div>');
        let arrTotal = syncCarousel.find('.item').length - 1;
        let item = '';
        let syncChild = $("#syncChild");
        for (let i = 0; i <= arrTotal; i++) {
            item = syncCarousel.find('.item').eq(i).find('img').attr('src');
            syncChild.append('<!-- Item ' + (i + 1) + '--><div class="item"><img src="' + item + '" alt=""></div>');
        }
    }

  
});