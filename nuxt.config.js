
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Wisdom Ezekiel',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/We.PNG' },
      {
        rel: "stylesheet",
        href:
        "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      },
    ],
    script:[
      {
        src: 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
        type: "text/javascript",
        body: true
      },
 
       {
          src: 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js ',
          type: "text/javascript",
          body: true
        },

        {
          src: " https://unpkg.com/sweetalert/dist/sweetalert.min.js",
          type: "text/javascript",
          // body: true
        },

        {
          src: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js",
          type: "text/javascript",
          body: true
        },
        
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    "~/assets/css/bootstrap.min.css",
    "~/assets/css/all.min.css",
    "~/assets/css/animate.min.css",
    '~/assets/css/owl.carousel.min.css',
    "~/assets/css/style.css"
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~/plugins/js/app.js', mode: 'client' },
    { src: '~/plugins/js/functions.js', mode: 'client' },
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  }
}
